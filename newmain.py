# -*- coding: utf-8 -*-

from mpi4py import MPI
import sys
from math import floor
from time import sleep
from random import random

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.size

def divisao(s,n,r):
	if r==0: return
	flr = floor(len(s)/(n-1))
	rest = len(s)%n
	return flr+1 if r<rest else flr

def distribuicao(s,n):
	pos = 0
	r = []
	for i in range(1,n):
		d = divisao(s,n,i)
		r.append(s[pos:pos+d])
		pos += d
	return r

def slicer():
	global size
	global rank
	x
	strlocal
	size_x = len(x)
	border = [0,0]			#booleano para definir se o processo terá bordas à esquerda ou à direita
	if rank == 1:			#se rank == 1, não tem vizinho à esquerda e portanto não terá borda esquerda
		border[0] = 1
	elif rank == size-1:	#se é o último rank, não tem vizinho à direita e portanto não terá borda direita
		border[1] = 1
	fila_ini = []			#fila com os índices que iniciam uma cadeia válida na borda do início
	ini = size_x-1			#variável que armazenará o índice que vai iniciar a substring do resto
	fila_end = []			#mesma atribuição da fila ini, mas para o final
	end = size_x-1			#variável que armazenará o índice que vai terminar a substring do resto
	achou = [0,0]			#variável pra indicar se haverá escrita no arquivo		
	del_vals = []			#lista de índices a serem removidos da fila (ini ou fim) após ser iterada	

	for i in range(2*size_x-1):	#esse espaço amostral é o menor possível onde, garantidamente, não cortaremos uma ocorrênxia de x ao meio
		#no mesmo loop, são verificadas as i-ésima e (n-i)-ésima posições na substring
		#aqui, para a i-ésima
		if not border[0]:							#se possui vizinho à esquerda...
			if strlocal[i] == x[0] and i < size_x:	#aqui, nós acrescentamos os índices onde, possivelmente, é iniciada uma ocorrência de x
				fila_ini.append(i)					#isso só é feito até a metade da substring, que equivalerá ao comprimento de x
			for j in fila_ini:						#aqui, nós iteramos na fila desses possíveis índices
				if x[i-j] != strlocal[i]:			#caso básico de deleção: a i-ésima posição partindo de j não corresponde à i-ésima de x
					del_vals.append(j)
				elif i-j == size_x-1:				#se o índice permaneceu na lista até o momento em que atingiu o tamanho de x, então
					ini = j 						#uma ocorrência de x foi encontrada. Portanto, a borda será tudo o que vier antes
					achou[0] = 1					#ao menos que uma outra ocorrência mais interna seja encontrada posteriormente
					del_vals.append(j)
			for k in del_vals:						#após o loop, os valores a serem removidos são removidos
				fila_ini.remove(k)
			del_vals.clear()
		#aqui, para a (n-1)-ésima. Mesma coisa de cima, só que de trás pra frente
		if not border[1]:
			if strlocal[(len(strlocal)-1)-i] == x[size_x-1] and i < size_x:
				fila_end.append(i)
			for j in fila_end:
				if x[(size_x-1)-(i-j)] != strlocal[(len(strlocal)-1)-i]:
					del_vals.append(j)
				elif i-j == size_x-1:
					end = j
					achou[1] = 1
			for k in del_vals:
				fila_end.remove(k)
			del_vals.clear()
	#dependendo de quais vizinhos o rank possui, cria as substrings com os resultados
	if border[0]:
		str_ini = "X"
		str_rest = strlocal[0:len(strlocal)-end]
		str_end = strlocal[len(strlocal)-end:len(strlocal)]
	elif border[1]:
		str_ini = strlocal[0:ini]
		str_rest = strlocal[ini:len(strlocal)]
		str_end = "X"
	else:
		str_ini = strlocal[0:ini]
		str_rest = strlocal[ini:len(strlocal)-end]
		str_end = strlocal[len(strlocal)-end:len(strlocal)]

	return [str_ini,str_rest,str_end]

if rank==0:
	try:
		out = open("trab-sd-bordas/dna.out","w")
	except:
		out = open("trab-sd-bordas/dna.out","x")
	q = open('trab-sd-bordas/query.in','r')
	for query in q:
		id_q = query
		x = q.readline().rstrip()
		if rank==0: out.write(id_q)
		f = open('trab-sd-bordas/dna.in','r')
		achou = 0
		for linha in f:
			id_str = linha
			string = f.readline().rstrip()
			print(id_str)
			print(string)
			tl = 1
			for s in range(1,size):
				tl += 1
				print('rank 0 envia mensagem de nova linha para rank',s,'atualizando seu tl para',tl)
				sleep(random())
				comm.send(["loop",tl],dest=s)
			dist = distribuicao(string,size)
			resp = 0
			resp_esperada = string.count(x)
			for s in range(1,size):
				tl += 1
				print('rank 0 envia string buscada e substring para rank',s,'atualizando seu tl para',tl)
				sleep(random())
				comm.send([x,dist[s-1],tl],dest=s)
			for s in range(1,size):
				infos = comm.recv(source=s)
				a = infos[0]
				print('rank 0 recebeu a resposta de rank',s,'\nTl de 0:',tl,'\nTl de',s,':',infos[1],'\nNova tl em 0 :',max(tl,infos[1]) + 1)
				tl = max(tl,infos[1]) + 1
				resp += a
			print("string procurada:",x,"\nid:",id_str)
			print("soma:",resp)
			if resp>0:
				achou = 1
				out.write(id_str+"{0}\n".format(resp)) 


			print("resposta esperada:",resp_esperada,"\n---------fim----------\n")
		if not bool(achou): out.write("NOT FOUND\n")
		f.close()
	for s in range(1,size):
		comm.send(["terminou",tl],dest=s)
	q.close()

else:
	rodando = True
	while(rodando):
		u = 0
		data = ''
		str_u = ''
		tl = 1
		infos = comm.recv(source=0)
		rodando = infos[0]
		sleep(random())
		print('rank',rank,'recebeu uma informação de nova linha de rank 0.\nTl de 0:',infos[1],'\nTl de',rank,':',tl,'\nNova tl em',rank,':',max(tl,infos[1]) + 1)
		tl = max(tl,infos[1]) + 1
		if rodando=="terminou": break
		infos = comm.recv(source=0)
		x = infos[0]
		strlocal = infos[1]
		sleep(random())
		print('rank',rank,'recebeu a string buscada e sua substring de rank 0.\nTl de 0:',infos[2],'\nTl de',rank,':',tl,'\nNova tl em',rank,':',max(tl,infos[2]) + 1)
		tl = max(tl,infos[2]) + 1
		#print('string recebida em rank',rank,':',strlocal)
		#receber a borda do vizinho anterior
		if not rank==1: 
			req = comm.irecv(source=rank-1)	
		r = slicer()											#separa a substring em três partes: borda esq, resto e borda dir
		ini = r[0]
		rest = r[1]
		end = r[2]
		if not rank==size-1:
			tl += 1
			sleep(random())
			print('rank',rank,'envia borda para rank vizinho, atualizando seu tl para',tl)
			snd = comm.isend([r[2],tl],dest=rank+1)				#envia a borda direita pro vizinho à direita, se tiver
		if not rank==1:											#se ele possui vizinho à esquerda, computa a ocorrência de substring
			data = req.wait()									#espera até a chegada da borda direita do vizinho à esquerda, se tiver
			str_u = data[0] + ini 									#da união de sua borda esquerda com a borda direita do vizinho à esquerda
			print('rank',rank,'recebeu a borda do rank',rank-1,'\nTl de ',rank-1,':',data[1],'\nTl de',rank,':',tl,'\nNova tl em',rank,':',max(tl,infos[2]) + 1)
			tl = max(tl,data[1]) + 1
			u = str_u.count(x)									#o valor é guardado em u
		n = rest.count(x)										#aqui, é computado o valor no resto (parte central) da substring
		if not rank==size-1: 
			snd.wait()											#garante que a borda direita foi enviada ao vizinho à direita
		n += u 													#armazena a soma das ocorrências no resto com as da união das bordas
		tl += 1
		print('rank',rank,'envia resposta encontrada para rank 0 atualizando seu tl para',tl)
		req = comm.send([n,tl], dest=0)								#envia essa soma para o rank 0, que vai somar todos esses resultados