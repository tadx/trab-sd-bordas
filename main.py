# -*- coding: utf-8 -*-

from mpi4py import MPI
import sys
from math import floor

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.size

#abrir ou criar novo arquivo de saída
if rank==0:
	try:
		out = open("dna.out","w")
	except:
		out = open("dna.out","x")

def divisao(s,n,r):
	if r==0: return
	flr = floor(len(s)/(n-1))
	rest = len(s)%n
	return flr+1 if r<rest else flr

#função que separa as bordas
def slicer():
	global size
	global rank
	global x
	global strlocal
	size_x = len(x)
	border = [0,0]			#booleano para definir se o processo terá bordas à esquerda ou à direita
	if rank == 1:			#se rank == 1, não tem vizinho à esquerda e portanto não terá borda esquerda
		border[0] = 1
	elif rank == size-1:	#se é o último rank, não tem vizinho à direita e portanto não terá borda direita
		border[1] = 1
	fila_ini = []			#fila com os índices que iniciam uma cadeia válida na borda do início
	ini = size_x-1			#variável que armazenará o índice que vai iniciar a substring do resto
	fila_end = []			#mesma atribuição da fila ini, mas para o final
	end = size_x-1			#variável que armazenará o índice que vai terminar a substring do resto
	achou = [0,0]			#variável pra indicar se haverá escrita no arquivo		
	del_vals = []			#lista de índices a serem removidos da fila (ini ou fim) após ser iterada	

	for i in range(2*size_x-1):	#esse espaço amostral é o menor possível onde, garantidamente, não cortaremos uma ocorrênxia de x ao meio
		#no mesmo loop, são verificadas as i-ésima e (n-i)-ésima posições na substring
		#aqui, para a i-ésima
		if not border[0]:							#se possui vizinho à esquerda...
			if strlocal[i] == x[0] and i < size_x:	#aqui, nós acrescentamos os índices onde, possivelmente, é iniciada uma ocorrência de x
				fila_ini.append(i)					#isso só é feito até a metade da substring, que equivalerá ao comprimento de x
			for j in fila_ini:						#aqui, nós iteramos na fila desses possíveis índices
				if x[i-j] != strlocal[i]:			#caso básico de deleção: a i-ésima posição partindo de j não corresponde à i-ésima de x
					del_vals.append(j)
				elif i-j == size_x-1:				#se o índice permaneceu na lista até o momento em que atingiu o tamanho de x, então
					ini = j 						#uma ocorrência de x foi encontrada. Portanto, a borda será tudo o que vier antes
					achou[0] = 1					#ao menos que uma outra ocorrência mais interna seja encontrada posteriormente
					del_vals.append(j)
			for k in del_vals:						#após o loop, os valores a serem removidos são removidos
				fila_ini.remove(k)
			del_vals.clear()
		#aqui, para a (n-1)-ésima. Mesma coisa de cima, só que de trás pra frente
		if not border[1]:
			if strlocal[(len(strlocal)-1)-i] == x[size_x-1] and i < size_x:
				fila_end.append(i)
			for j in fila_end:
				if x[(size_x-1)-(i-j)] != strlocal[(len(strlocal)-1)-i]:
					del_vals.append(j)
				elif i-j == size_x-1:
					end = j
					achou[1] = 1
			for k in del_vals:
				fila_end.remove(k)
			del_vals.clear()
	#dependendo de quais vizinhos o rank possui, cria as substrings com os resultados
	if border[0]:
		str_ini = "X"
		str_rest = strlocal[0:len(strlocal)-end]
		str_end = strlocal[len(strlocal)-end:len(strlocal)]
	elif border[1]:
		str_ini = strlocal[0:ini]
		str_rest = strlocal[ini:len(strlocal)]
		str_end = "X"
	else:
		str_ini = strlocal[0:ini]
		str_rest = strlocal[ini:len(strlocal)-end]
		str_end = strlocal[len(strlocal)-end:len(strlocal)]

	return [str_ini,str_rest,str_end]

#loops pegando cada cadeia para cada query
q = open('query.in','r')
for query in q:
	id_q = query
	x = q.readline().rstrip()
	if rank==0: out.write(id_q)
	f = open('dna.in','r')
	achou = 0
	for linha in f:
		id_str = linha
		string = f.readline()
		strlocal = divisao(string,size,rank)					#comprimento de cada subsequência da string
		if rank == 0:											#o rank zero somente recebe a resposta dos outros ranks e agrega
			resp = 0
			resp_esperada = string.count(x)
			for s in range(1,size):
				a = comm.recv(source=s)
				resp += a
			print("string procurada:",x,"\nid:",id_str)
			print("soma:",resp)
			if resp>0:
				achou = 1
				out.write(id_str+"{0}\n".format(resp)) 


			print("resposta esperada:",resp_esperada,"\n---------fim----------\n")


		else:
			data = ''
			u = 0
			if not rank==1: req = comm.irecv(source=rank-1)			#chamada não bloqueante do resultado da borda à esquerda
			inistr = int((rank-1)*strlocal)
			endstr = int((rank*strlocal))
			strlocal = string[inistr:endstr]						#pega a substring local
			r = slicer()											#separa a substring em três partes: borda esq, resto e borda dir
			ini = r[0]
			rest = r[1]
			end = r[2]
			if not rank==size-1: snd = comm.isend(r[2],dest=rank+1)	#envia a borda direita pro vizinho à direita, se tiver
			if not rank==1: data = req.wait()						#espera até a chegada da borda direita do vizinho à esquerda, se tiver
			if not rank==1:											#se ele possui vizinho à esquerda, computa a ocorrência de substring
				str_u = data + ini 									#da união de sua borda esquerda com a borda direita do vizinho à esquerda
				u = str_u.count(x)									#o valor é guardado em u
			n = rest.count(x)										#aqui, é computado o valor no resto (parte central) da substring
			if not rank==size-1: snd.wait()							#garante que a borda direita foi enviada ao vizinho à direita
			n += u 													#armazena a soma das ocorrências no resto com as da união das bordas
			req = comm.send(n, dest=0)								#envia essa soma para o rank 0, que vai somar todos esses resultados
	if rank==0: 
		if not bool(achou): out.write("NOT FOUND\n")
	f.close()
q.close()